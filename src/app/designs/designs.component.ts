import { Component } from '@angular/core';

@Component({
  selector: 'app-designs',
  templateUrl: './designs.component.html',
  styleUrls: ['./designs.component.css']
})
export class DesignsComponent {
  public imagesUrl;


  ngOnInit() {
    this.imagesUrl = [{
      image: 'assets/images/1.jpg',
      thumbImage: 'assets/images/1.jpg'
    },
    {
      image: 'assets/images/2.jpg',
      thumbImage: 'assets/images/2.jpg'
    },
    {
      image: 'assets/images/3.jpg',
      thumbImage: 'assets/images/3.jpg'
    },
    {
      image: 'assets/images/4.jpg',
      thumbImage: 'assets/images/4.jpg'
    },
    {
      image: 'assets/images/5.jpg',
      thumbImage: 'assets/images/5.jpg'
    },
    {
      image: 'assets/images/6.jpg',
      thumbImage: 'assets/images/6.jpg'
    },
    {
      image: 'assets/images/7.jpg',
      thumbImage: 'assets/images/7.jpg'
    }];
  }

}
