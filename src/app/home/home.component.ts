import { Component} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  public imagesUrl;
  ngOnInit(){
    this.imagesUrl = [{
      image: 'assets/images/lady1.jpg',
      thumbImage: 'assets/images/lady1.jpg'
    },
    {
      image: 'assets/images/lady2.jpg',
      thumbImage: 'assets/images/lady2.jpg'
    },
    {
      image: 'assets/images/lady3.jpg',
      thumbImage: 'assets/images/lady3.jpg'
    }];

  }

}
