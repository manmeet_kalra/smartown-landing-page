.PHONY: build pack upload

TAG?=$(shell git rev-list HEAD --max-count=1 --abbrev-commit)

export TAG

build: 
	npm run build
pack:build
	docker build -t 452171489035.dkr.ecr.us-east-1.amazonaws.com/smartown-website:$(TAG) .
upload:pack
	docker push 452171489035.dkr.ecr.us-east-1.amazonaws.com/smartown-website:$(TAG)

